import type { Todo } from "@repo/shared";
import { useEffect, useState } from "react";

export function App() {
  const [todos, setTodos] = useState<Todo[]>([]);

  useEffect(() => {
    loadTodos();
  }, []);

  function loadTodos() {
    fetch("http://localhost:3000/todos")
      .then((res) => res.json())
      .then((todos) => setTodos(todos));
  }

  function handleToggleTodo(todoId: number) {
    fetch(`http://localhost:3000/todos/${todoId}`, {
      method: "PATCH",
    })
      .then(() => {
        loadTodos();
      })
      .catch(() => alert("deu ruim"));
  }

  function handleAddTodo(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const todoTitle = formData.get("title");
    if (!todoTitle) return;

    fetch("http://localhost:3000/todos", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        title: todoTitle,
      }),
    })
      .then(() => {
        loadTodos();
      })
      .catch(() => alert("deu ruim"));
  }

  function handleDeleteTodo(todoId: number) {
    fetch(`http://localhost:3000/todos/${todoId}`, {
      method: "DELETE",
    })
      .then(() => {
        loadTodos();
      })
      .catch(() => alert("deu ruim"));
  }

  return (
    <div>
      <ul>
        {todos.map((todo) => (
          <li
            key={todo.id}
            style={{
              textDecoration: todo.completed ? "line-through" : "none",
              cursor: "pointer",
            }}
            onClick={() => handleToggleTodo(todo.id)}
          >
            {todo.title} -{" "}
            <button
              onClick={(e) => {
                e.stopPropagation();
                handleDeleteTodo(todo.id);
              }}
            >
              remove
            </button>
          </li>
        ))}
      </ul>
      <form onSubmit={handleAddTodo}>
        <input type="text" name="title" id="title" />
        <button type="submit">Add Todo</button>
      </form>
    </div>
  );
}
