import { TodoMap, Todo } from "@repo/shared";
import { fastify as createFastify } from "fastify";
import cors from "@fastify/cors";

const fastify = createFastify({
  logger: true,
});

await fastify.register(cors, {});

const todoMap: TodoMap = new Map();

fastify.get("/todos", async () => {
  return Array.from(todoMap.values());
});

fastify.post<{ Body: Pick<Todo, "title"> }>("/todos", async (request) => {
  const { title } = request.body;
  const todo: Todo = {
    id: Date.now(),
    title,
    completed: false,
  };
  todoMap.set(todo.id, todo);
  return todo;
});

fastify.patch<{ Params: { id: string } }>(
  "/todos/:id",
  async (request, reply) => {
    const { id } = request.params;
    const todoId = Number(id);
    if (todoMap.has(todoId)) {
      const todo = todoMap.get(todoId);
      todo!.completed = !todo?.completed;
      return todo;
    }
    return reply.code(404).send();
  }
);

fastify.delete<{ Params: { id: string } }>(
  "/todos/:id",
  async (request, reply) => {
    const { id } = request.params;
    if (todoMap.has(Number(id))) {
      todoMap.delete(Number(id));
      return reply.code(204).send();
    }
    return reply.code(404).send();
  }
);

// Run the server!
const start = async () => {
  try {
    await fastify.listen({ port: 3000 });
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
