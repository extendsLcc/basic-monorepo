type Todo = { id: number; title: string; completed: boolean };

type TodoMap = Map<number, Todo>;

export type { Todo, TodoMap };
